def max5(num: int) -> int:
    snum = str(abs(num))
    if num < 0:
        pred = lambda d: d > 5
        sign = '-'
        idx = 0
    else:
        pred = lambda d: d <= 5
        sign = ''
        idx = len(snum) 
    digits = [d for d in str(abs(num))] 
    print(list(enumerate(digits)))
    for i, v in enumerate(digits):
        if pred(int(v)):
            idx = i
            break
    print(idx)
    digits.insert(idx, '5')
    return int(sign + ''.join(digits))
