import pytest
from main import max5

def test_268_to_5268():
    assert 5268 == max5(268)

def test_0_to_50():
    assert 50 == max5(0)

def test_5_to_55():
    assert 55 == max5(5)

def test_1_to_51():
    assert 51 == max5(1)

def test_neg1_to_neg15():
    assert -15 == max5(-1)

def test_50_to_550():
    assert 550 == max5(50)

def test_neg50_to_neg505():
    assert -505 == max5(-50)

def test_505_to_5505():
    assert 5505 == max5(505)

def test_neg505_to_neg5055():
    assert -5055 == max5(-505)

def test_6_to_65():
    assert 65 == max5(6)

def test_neg6_to_neg56():
    assert -56 == max5(-6)

def test_65_to_655():
    assert 655 == max5(65)

def test_9876543210_to_9876554321():
    assert 98765543210 == max5(9876543210)

def test_neg999_to_neg5999():
    assert -5999 == max5(-999)
